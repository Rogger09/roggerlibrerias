/**
 * @author Rogger
 * @version 1.0
 * @since 07/03/2019
 */

package proyectoJunitRogger;

import java.util.Scanner;

public class JunitLibrerias {

	static Scanner reader = new Scanner(System.in);
	
	static int llibres = 0;
	static int estanterias = 0;

	public static void main(String[] args) {
		
		/**
		 * Crear variables para guardar el n�mero de huecos y el n�mero de libros
		 */
		
		int huecos;
		int num;

		System.out.println("Cuantos huecos quieres que tenga la libreria?"); 
		huecos = reader.nextInt();
		
		/**
		 * Comprobar que se introduzca un n�mero positivo y que este entre 1 y 1000
		 */
		
		if (huecos <= 0 || huecos > 1000) {
			System.out.println("Tiene que ser un n�mero positivo entre 1 y 1000");
		} else {
			System.out.println("Introduce la cantidad de libros: ");
			num = reader.nextInt();

			if (num <= 0 || num > 500) {
				System.out.println("Tienes que introducir un n�mero positivo entre 1 y 500.");
				
			} else {
				librerias(huecos, num);
			}
		}
	}
	
	/**
	 * Bucle para ir rellenando los huecos de las estanterias con libros y decirnos los que sobran.
	 * @param huecos Espacios en las libreias para los libros
	 * @param num Numero total de libros para guardar en las estanterias
	 */

	public static void librerias(int huecos, int num) {

		for (int i = 0; i < num; i++) {

			llibres++;

			if (llibres == huecos) {
				estanterias++;
				llibres = 0;
			}
		}
		System.out.println("Has llenado: " + estanterias + " estanterias");
		System.out.println("Te han sobrado un total de:  " + llibres + " libros.");
	}

}